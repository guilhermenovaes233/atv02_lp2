<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'pagina';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['cursos'] = 'pagina/cursos';
$route['elements'] = 'pagina/elements';
$route['main'] = 'pagina/main';
$route['about'] = 'pagina/about';
$route['blog'] = 'pagina/main';
$route['courses_details'] = 'pagina/courses_details';
$route['single_blog'] = 'pagina/single_blog';
$route['viewProgressBar'] = 'pagina/viewProgressBar';
$route['viewPagination'] = 'pagina/viewPagination';
$route['viewPanel'] = 'pagina/viewPanel';
$route['contato'] = 'pagina/contato';

