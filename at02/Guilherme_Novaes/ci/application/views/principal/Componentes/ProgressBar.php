
     <!-- bradcam_area_start -->
     <div class="courses_details_banner">
         <div class="container">
             <div class="row">
                 <div class="col-xl-6">
                     <div class="course_text">
                            <h3>Progress Bar</h3>
                            <div class="rating">
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <span>(5)</span>
                            </div>
                            <div class="hours">
                                <div class="video">
                                     <div class="single_video">
                                            <i class="fa fa-clock-o"></i> 
                                            <span> Videos</span>
                                     </div>
                                </div>
                            </div>
                     </div>
                 </div>
             </div>
         </div>
    </div>
    <!-- bradcam_area_end -->

    <div class="courses_details_info">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="single_courses">
                    <h3 class="second_title">Dicas</h3>
                    </div>
                    <div class="outline_courses_info">
                            <div id="accordion">
                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    <i class="flaticon-question"></i> De onde esse conteúdo foi tirado?
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <div class="card-body">
                                               Esse conteúdo pode ser encontrado no site 
                                               <a href="https://getbootstrap.com.br/docs/4.1/components/progress/#como-funciona"> <u> Getbootstrap </u>  </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                    <i class="flaticon-question"></i>Basic Classes</span>
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                            <div class="card-body">
                                                Our set he for firmament morning sixth subdue darkness creeping gathered divide our
                                                let god moving. Moving in fourth air night bring upon
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5">
                    <div class="courses_sidebar">
                        <div class="video_thumb">
                            <img src="assets/img/courses/progress-bar-square.jpg" alt="">
                            <a class="popup-video" href="https://www.youtube.com/watch?v=093pKz4YzY4">
                                <i class="fa fa-play"></i>
                            </a>
                        </div>
                        <div class="author_info">
                            <div class="auhor_header">
                                <div class="thumb">
                                    <img src="assets/img/team/Guilherme1.jpg" style="width: 50%;" alt="">
                                </div>
                                <div class="name">
                                    <h3>Guilherme Novaes</h3>
                                    <p>Desenvolvedor Full Stack</p>
                                </div>
                            </div>
                            <p class="text_info">
                                Estudante do curso de análise e desenvolvimento de sistemas
                            </p>
                            <ul>
                                <li><a href="#"> <i class="fa fa-envelope"></i> </a></li>
                                <li><a href="#"> <i class="fa fa-twitter"></i> </a></li>
                                <li><a href="#"> <i class="ti-linkedin"></i> </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

    <br/><br/>
    <div class="single_courses">
        <h3> Como funciona </h3>
        <p> Componentes de progresso são feitos com dois elementos HTML, um pouco de CSS para a largura e outros atributos. Não usamos o elemento HTML5 progress
        para garantir que você possa empilhar barras de progresso e colocar textos sobre elas.</p>
    </div>
    <br/>
    <div class="single_courses">
        <?= $exemplo1 ?> 
    <br/>

    <textarea readonly style="width: 100%; height: 400px;"> 
        <?= $exemplo1 ?> 
        <br/>
    </textarea> 

    </div>
    <br/>          

    <div class="single_courses">
        <h3> Rótulos </h3>
        <p> Use rótulos em suas barras de progressos, colocando texto dentro do elemento com .progress-bar.</p>
        <br/>
        <div class="progress">
            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
        </div>
        <br/>
        <textarea readonly style="width: 100%; height: 100px;"> 
            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
            </div>
        </textarea> 
        <br/>
    </div>
    <br/>

    <div class="single_courses">
        <h3> Altura </h3>
        <p> Nós só definimos um valor para altura no .progress, então, se você alterar o valor, 
            o elemento interno .progress-bar vai se redimensionar 
            propocionalmente, automaticamente.</p>
        <br/>

        <div class="single_courses">
            <?= $exemplo5 ?> 
        <br/>   

        <textarea readonly style="width: 100%; height: 200px;"> 
            <?= $exemplo5 ?> 
        </textarea> 
        <br/>
    </div>

    <div class="single_courses">
        <h3> Backgrounds </h3>
        <p> Use classes utilitárias de background para mudar a aparência das barras de progresso. </p>
        <br/>
            <div class="single_courses">
                <?= $exemplo2 ?> 
            <br/>
        <br/>

    <textarea readonly style="width: 100%; height: 350px;"> 
        <?= $exemplo2 ?> 
    </textarea> 
    <br/>
    </div>

    <div class="single_courses">
        <h3> Múltiplas barrras </h3>
        <p> Crie várias barras de progresso, dentro de um único componente de progresso, se precisar. </p>
        <br/>
        <div class="progress">
            <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
            <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
            <div class="progress-bar bg-info" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
        </div>

        <textarea readonly style="width: 100%; height: 100px;"> 
            <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
            <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
            <div class="progress-bar bg-info" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
        </textarea> 
        <br/>
    </div><br/>

    <div class="single_courses">
        <h3> Listrada </h3>
        <p> Adicione .progress-bar-striped em qualquer 
            .progress-bar para aplicar uma listra sobre a cor de fundo, via gradiente CSS.</p>
        <br/>
        <div class="single_courses">
            <?= $exemplo3 ?> 
            <br/>
        </div>
        
        <textarea readonly style="width: 100%; height: 400px;"> 
            <?= $exemplo3 ?> 
        </textarea> 
        <br/>
        <br/>
    </div>
    
    <div class="single_courses">
        <h3> Listras animadas </h3>
        <p> O gradiente listrado também pode ser animado. Use .progress-bar-animated no 
            .progress-bar para animar as listras, da direita para esquerda, via animações CSS3.</p>
        <br/>
        <div class="single_courses">
            <?= $exemplo4 ?> 
            <br/>
        </div>
        <textarea readonly style="width: 100%; height: 100px;"> 
            <?= $exemplo4 ?> 
            <br/>
        </textarea> 
        <br/>
    </div>
</div>
</div>
</div>
</div>
</div>
