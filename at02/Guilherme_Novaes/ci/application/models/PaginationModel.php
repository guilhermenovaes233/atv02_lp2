<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Inclui a classe da Pagination
 */
include APPPATH.'libraries/componentes/PaginationItem.php';

/**
 * Classe da model Pagination
 * 
 * @Author Guilherme Novaes
 * 
 */
class PaginationModel extends CI_Model{

    /**
     * Função pública para gerar e recuperar uma pagination para o exemplo inicial
     */
    public function getComponente(){
            $data = new PaginationItem('pagination');
            $lista = $data->getComponente();

        return $lista;
    }

    /**
     * Função pública para gerar e recuperar uma pagination para o exemplo 2
     */
    public function getComponente2(){
        $data = new PaginationItem('pagination justify-content-center');
        $lista = $data->getComponente();

    return $lista;
    }

    /**
     * Função pública para gerar e recuperar uma pagination para o exemplo 3
     */
    public function getComponente3(){
        $data = new PaginationItem('pagination pagination-lg');
        $lista = $data->getComponente();

    return $lista;
    }

    /**
     * Função pública para gerar e recuperar uma pagination para o exemplo 4
     */
    public function getComponente4(){
        $data = new PaginationItem('pagination');
        $lista = $data->getComponente1();

    return $lista;
    }

    /**
     * Função pública para gerar e recuperar uma pagination para o exemplo 5
     */
    public function getComponente5(){
        $data = new PaginationItem('pagination');
        $lista = $data->getComponente2();

    return $lista;
    }
}