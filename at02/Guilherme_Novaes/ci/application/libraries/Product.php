<?php 
/**
 * Classe que representa os produtos do e-commerce da empresa ifsp
 */

class Product{

    private $nome;
    private $preco = 0;

    function __construct($nome){
        $this->nome = $nome;
    }

    /**
     * Informa o nome do produto
     * @return nome: string ou -1 em caso de Erro
     */
    function getNome(){
       $size = strlen($this->nome);
       return $size >= 3 ? $this->nome : -1;
       //return null;
    }

    /**
     * Atribui preço ao produto
     * @param preco: float
     */
    function SetPreco($preco){
        $this->preco = $preco;
    }

    function getPreco(){
        //return 1;
        return $this->preco;
    }

     /**
      * Calcula um desconto percentual sobre o preco do produto
      *@param percent: inteiro
      *@return desconto: float
      */

     function desconto($percent){
        $desc = $this->preco * ( $percent/100 );
        return $desc;
     }
}