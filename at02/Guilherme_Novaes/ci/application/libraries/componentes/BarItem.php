<?php
/**
 * Classe do componente ProgressBar
 * 
 * @Author Guilherme Novaes
 * 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Classe de geração do componente Progress Bar
 */
class BarItem{
    
    /**
     * Atributo Privado que recebe o nome da classe 
     */
    private $classe;
    /**
     * Atributo Privado que recebe o tamanho da classe 
     */
    private $tamanho;
    /**
     * Atributo Privado que recebe o tamanho da classe 
     */
    private $rotulo = '';

    /**
     * Inicializa todos atributos com seus valores padrões.
     */
    function __construct($classe, $tamanho, $rotulo){
        $this->classe = $classe;
        $this->tamanho = $tamanho;
        $this->rotulo = $rotulo;
    }

    /**
     * Método público que gera o componente Progress Bar e o retorna como HTML
     */
    public function getComponente(){
        $html = '<div class="progress" '.$this->rotulo.'>
                    <div class="'.$this->classe.'" role="progressbar" 
                    style="width: '.$this->tamanho.'%" aria-valuenow="'.$this->tamanho.'" aria-valuemin="0" aria-valuemax="100"></div>
                </div>';
                
        return $html;
    }
} 