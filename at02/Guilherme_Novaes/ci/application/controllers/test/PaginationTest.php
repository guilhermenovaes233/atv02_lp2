<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/test/Toast.php');

include_once APPPATH.'libraries/componentes/PaginationItem.php';

class PaginationTest extends Toast{ 
    function __construct() {
		parent::__construct('PaginationItemTeste');
    }	
    
    // Caso de teste 1
    function test_objeto(){
        $prod = new PaginationItem('pagination');
        $html = $prod->getComponente();
        $result = $html;
        $this->_assert_not_empty( $result, "Erro: Não pode estar vazio" );
    }
}