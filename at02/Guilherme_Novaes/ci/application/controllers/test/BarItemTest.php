<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/test/Toast.php');

include_once APPPATH.'libraries/componentes/BarItem.php';

class BarItemTest extends Toast{ 
    function __construct() {
		parent::__construct('BarItemTeste');
    }	
    
    // Caso de teste 1
    function test_objeto(){
        $prod = new BarItem('progress-bar', 25, '');
        $html = $prod->getComponente();
        $result = $html;
        $this->_assert_not_empty( $result, "Erro: Não pode estar vazio" );
    }
}