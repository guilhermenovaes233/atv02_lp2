<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'controllers/test/Toast.php');

include_once APPPATH.'libraries/componentes/PanelItem.php';

class PanelItemTest extends Toast{ 
    function __construct() {
		parent::__construct('PanelItemTeste');
    }	
    
    // Caso de teste 1
    function test_objeto(){
        $prod = new PanelItem('panel-default');
        $html = $prod->getComponente1();
        $result = $html;
        $this->_assert_not_empty( $result, "Erro: Não pode estar vazio" );
    }
}