<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pagina extends CI_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper('url');
		$this->load->model('BarModel', 'bar');
		$this->load->model('PanelModel', 'panel');
		$this->load->model('PaginationModel', 'pag');
    }

	public function index()
	{
		$this->load->view('common/header');
		$this->load->view('common/topo');
		$this->load->view('principal/index');
		$this->load->view('common/footer');
    }

    public function about()
	{
		$this->load->view('common/header');
		$this->load->view('common/topo');
		$this->load->view('principal/about');
		$this->load->view('common/footer');
	}

	public function blog()
	{
		$this->load->view('common/header');
		$this->load->view('common/topo');
		$this->load->view('principal/blog');
		$this->load->view('common/footer');
	}
	

	public function viewPanel()
	{
		$this->load->view('common/header');
		$this->load->view('common/topo');

		$info['exemplo1'] = $this->panel->getComponente();
		$info['exemplo2'] = $this->panel->getExemplo2();
		$info['exemplo3'] = $this->panel->getExemplo3();

		$info['exemplo4'] = $this->panel->getComponentes();
		
		$this->load->view('principal/componentes/Panel', $info);
		$this->load->view('common/footer');
	}

	public function viewProgressBar()
	{
		$this->load->view('common/header');
		$this->load->view('common/topo');

		$info['exemplo1'] = $this->bar->getComponente();
		$info['exemplo2'] = $this->bar->getComponente1();
		$info['exemplo3'] = $this->bar->getComponente2();
		$info['exemplo4'] = $this->bar->getComponente3();
		$info['exemplo5'] = $this->bar->getComponente4();

		$this->load->view('principal/componentes/ProgressBar', $info);
		$this->load->view('common/footer');
	}

	public function viewPagination()
	{
		$this->load->view('common/header');
		$this->load->view('common/topo');
		$info['exemplo1'] = $this->pag->getComponente();
		$info['exemplo2'] = $this->pag->getComponente2();
		$info['exemplo3'] = $this->pag->getComponente3();
		$info['exemplo4'] = $this->pag->getComponente4();
		$info['exemplo5'] = $this->pag->getComponente5();
		$this->load->view('principal/componentes/Pagination', $info);
		$this->load->view('common/footer');
	}

	public function cursos()
	{
		$this->load->view('common/header');
		$this->load->view('common/topo');
		$this->load->view('principal/courses');
		$this->load->view('common/footer');
	}
	public function elements()
	{
		$this->load->view('common/header');
		$this->load->view('common/topo');
		$this->load->view('principal/elements');
		$this->load->view('common/footer');
	}
	public function main()
	{
		$this->load->view('common/header');
		$this->load->view('common/topo');
		$this->load->view('principal/main');
		$this->load->view('common/footer');
	}

	public function single_blog()
	{
		$this->load->view('common/header');
		$this->load->view('common/topo');
		$this->load->view('principal/single_blog');
		$this->load->view('common/footer');
	}

	public function contato()
	{
		$this->load->view('common/header');
		$this->load->view('common/topo');
		$this->load->view('principal/contato');
		$this->load->view('common/footer');
	}
}
